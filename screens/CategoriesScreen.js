import React from 'react';
import {FlatList, StyleSheet} from 'react-native';

import {CATEGORIES} from '../data/dummy-data'; //data yg diperlukan kategori
import CategoryGridTile from '../components/CategoryGridTile'; //komponen

const CategoriesScreen = props => {
  const renderGridItem = itemData => {
    return (
      //ini cara apply komponen
      <CategoryGridTile
        title={itemData.item.title}
        color={itemData.item.color}
        onSelect={() => {
          props.navigation.navigate({
            routeName: 'CategoryMeals',
            params: {
              categoryId: itemData.item.id,
            },
          });
        }}
      />
    );
  };

  return (
    //ini nampilin data/render data dalam bentuk flat list
    <FlatList
      data={CATEGORIES}
      numColumns={2}
      renderItem={renderGridItem}
      keyExtractor={(item, index) => item.id}
    />
  );
};

//css
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default CategoriesScreen;
