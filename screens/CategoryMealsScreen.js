import React from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';

import {CATEGORIES, MEALS} from '../data/dummy-data'; //data
import Color from '../constants/Color'; //default color
import MealItem from '../components/MealItem'; //komponen meal

const CategoryMealsScreen = props => {
  const renderMealItem = itemData => {
    return (
      //komponen
      <MealItem
        title={itemData.item.title}
        duration={itemData.item.duration}
        complexity={itemData.item.complexity}
        affordability={itemData.item.affordability}
        image={itemData.item.imageUrl}
        onSelectMeal={() => {
          props.navigation.navigate({
            routeName: 'MealDetail',
            params: {
              mealId: itemData.item.id,
            },
          });
        }}
      />
    );
  };

  //ini cara getparam dari navigator categoriesScreen.js
  const catId = props.navigation.getParam('categoryId');

  //difilter
  const displayMeals = MEALS.filter(
    meal => meal.categoryIds.indexOf(catId) >= 0,
  );

  return (
    //nampilin data
    <View style={styles.screen}>
      <FlatList
        data={displayMeals}
        keyExtractor={(item, index) => item.id}
        renderItem={renderMealItem}
        style={{width:'100%'}}
      />
    </View>
  );
};

CategoryMealsScreen.navigationOptions = navigationData => {
  const catId = navigationData.navigation.getParam('categoryId');

  const selectedCat = CATEGORIES.find(cat => cat.id === catId);

  return {
    headerTitle: selectedCat.title,
    headerStyle: {
      backgroundColor: Color.primaryColor,
    },
    headerTintColor: 'white',
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default CategoryMealsScreen;
