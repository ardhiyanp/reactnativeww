Readme React-native :
===============================================================================
 - Untuk run aplikasi code nya semua di import kan di App.js

Folder navigation :
===============================================================================
 - Main Navigation ada di folder navigation/MealsNavigator -> di import ke App.js untuk main home root navigation
 - Ada keterangan di dalem nya

Folder models + data :
===============================================================================
 - di models cuman defined dataset aja
 - isinya ada di data
 - tiap data yg diperlukan dipanggil di page di dalam folder screen

Folder components
================================================================================
 - sama seperti komponen pada umumnya

Folder screens
================================================================================
 - Berisi dari page yg kita buat
 - di categoriesScreen, categoryMeals sudah ada contoh penerapan komponen didalamnya

Folder constants
================================================================================
 - defined default color/stylesheet

Tambahan
================================================================================
react-native.config.js => buat ngelink otomatis semisal kita nambah fonts,ada keterangan di docs web nya