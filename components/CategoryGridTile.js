import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
} from 'react-native';

const CategoryGridTile = props => {
  let TouchCmp = TouchableOpacity;

  if (Platform.OS === 'android' && Platform.Version >= 21) {
    TouchCmp = TouchableNativeFeedback;
  }
  return (
    <View style={style.gridItem}>
      <TouchCmp style={{flex: 1}} onPress={props.onSelect}>
        <View style={{...style.container, ...{backgroundColor: props.color}}}>
          <Text style={style.title} numberOfLines={2}>
            {props.title}
          </Text>
        </View>
      </TouchCmp>
    </View>
  );
};

const style = StyleSheet.create({
  gridItem: {
    flex: 1,
    margin: 15,
    height: 150,
    borderRadius: 10,
    overflow: 'hidden',
  },
  container: {
    flex: 1,
    borderRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 3,
    padding: 15,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  title: {
    fontFamily: 'MontserratRegular',
    fontSize: 18,
    textAlign: 'right',
  },
});

export default CategoryGridTile;
