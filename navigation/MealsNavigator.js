import {createStackNavigator} from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Color from '../constants/Color';

import CategoriesScreen from '../screens/CategoriesScreen'; //import component view dari folder screens/
import CategoryMealsScreen from '../screens/CategoryMealsScreen'; //import component view dari folder screens/
import MealDetailScreen from '../screens/MealDetailScreen'; //import component view dari folder screens/

const MealsNavigator = createStackNavigator(
  {
    Categories: {
      screen: CategoriesScreen, //semua code dari file ini otomatis nanti tampil di page categoriesScreens
      navigationOptions: {
        headerTitle: 'Meal Categories',
      },
    },
    CategoryMeals: {
      screen: CategoryMealsScreen,
    },
    MealDetail: MealDetailScreen,
  },
  {
    //deafult styling header navigation color top
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: Color.primaryColor,
      },
      headerTintColor: 'white',
    },
  },
);

export default createAppContainer(MealsNavigator);